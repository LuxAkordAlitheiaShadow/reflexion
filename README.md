# Reflexion

-------------------------------------------------

## Introduction

This scholar project is an example of meta-programming concept highlighted by creating instance of car.

-------------------------------------------------

## Ressources

The UML diagrams of this project is in `/Resources/UML` folder in `.vpp` format (Visual Paradigm) and also in format `.pnj`.

All screenshot are in `/Resources/Screenshots` folder.

-------------------------------------------------

## Running project

This project was developed with IntelliJ with the compiler OpenJDK17.

You just need to run the Main.java with an IDE or compile the project to an executable.

-------------------------------------------------

## Features

Two types of Car can be created : basic Car, for whom we can set the speed of the car, and Sport Car, for whom the speed is set to 200 speed unit.

Car can be created with 3 modes of construction :

- Instantiation
- Meta
- Reflexion

For each mode, one Sport Car and two basic Car with different speed are created. 
Then we do 20 cycles where each car move, according to their speed, and are watched when they're exceeded 1000 position unit.

The watcher throw an exception when a car moving too fast, by default 60 speed unit.

-------------------------------------------------

## Screenshot

Here a screenshot of the execution.

![Execution](./Ressources/Screenshots/Execution.png)