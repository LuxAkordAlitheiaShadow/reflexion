package com.lux.Reflexion.car;

public class Car
{
    //Attributes.
    protected int speed;
    protected int position;
    protected int id;
    protected static int _id=0;

    // Constructor.
    public Car(int speed)
    {
        this._id++;
        this.id = this._id;
        this.position = 0;
        this.speed = speed;
    }

    // Getters.
    public int getPosition()
    {
        return this.position;
    }

    public int getId()
    {
        return this.id;
    }

    // Methods.
    public void move()
    {
        this.position += this.speed;
    }

    public String toString()
    {
        return "Speed : "+this.speed+", position : "+this.position+", id : "+this.id;
    }
}
