package com.lux.Reflexion.car;

public interface Watchable
{
    public int watch(int limit);
}
