package com.lux.Reflexion.car;

public class MetaCarSport extends CarSport implements Watchable
{

    //Constructeur
    public MetaCarSport()
    {
        this.id = this._id;
        this.position = 0;
        this.speed = 200;
    }

    public int watch(int limit)
    {
        return this.speed - limit;
    }

}