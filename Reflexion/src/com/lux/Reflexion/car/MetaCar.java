package com.lux.Reflexion.car;

public class MetaCar extends Car implements Watchable
{
    //Constructeur
    public MetaCar(int speed)
    {
        super(speed);
        this.id = this._id;
        this.position = 0;
    }

    //Methodes
    public int watch(int limit)
    {
        return this.speed - limit;
    }

}
