package com.lux.Reflexion.car;

import java.lang.reflect.InvocationTargetException;

public class CarFactory
{
    public enum ConstructionMode
    {
        INSTANTIATION,
        REFLEXION,
        META
    };

    // Methods.

    // Returning a Car instance according to the construction mode
    public static Car buildCar(ConstructionMode mode, boolean sport, int speed)
    {
        // Declaring variable
        Car newCar = null;
        // Construction mode statement
        switch (mode)
        {
            case INSTANTIATION:
                newCar = buildCarInstantiation(sport, speed);
                break;
            case META:
                newCar = buildCarMeta(sport, speed);
                break;
            case REFLEXION:
                newCar = buildCarReflexion(sport, speed);
                break;
            default:
                System.out.println("Error | mode doesn't match in buildCar function");
        }

        return newCar;
    }

    // Retuning Car instance by instantiation
    private static Car buildCarInstantiation(boolean sport, int speed)
    {
        Car newCar;
        // Sport car statement
        if ( sport )
        {
            newCar = new CarSport();
        }
        // Basic car statement
        else
        {
            newCar = new Car(speed);
        }
        return newCar;
    }

    // Returning Car instance by meta
    private static Car buildCarMeta(boolean sport, int speed)
    {
        try
        {
            // Declaring variables
            Class className;
            Object object;
            // Sport car statement
            if (sport)
            {
                // Getting class
                className = Class.forName(MetaCarSport.class.getName());
                // Getting class instance
                object = className.getDeclaredConstructor().newInstance();
            }
            // Basic car statement
            else
            {
                // Getting class
                className = Class.forName(MetaCar.class.getName());
                // Setting parameters
                Class[] parameters = new Class[1];
                parameters[0] = int.class;
                // Getting class instance
                object = className.getDeclaredConstructor(parameters).newInstance(speed);
            }
            return (Car) object;
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException buildCarReflexionException)
        {
            buildCarReflexionException.printStackTrace();
        }
        return null;
    }

    // Returning Car instance by reflexion
    private static Car buildCarReflexion(boolean sport, int speed)
    {
        try
        {
            // Declaring variables
            Class className;
            Object object;
            // Sport car statement
            if (sport)
            {
                // Getting class
                className = Class.forName(CarSport.class.getName());
                // Getting class instance
                object = className.getDeclaredConstructor().newInstance();
            }
            // Basic car statement
            else
            {
                // Getting class
                className = Class.forName(Car.class.getName());
                // Setting parameters
                Class[] parameters = new Class[1];
                parameters[0] = int.class;
                // Getting class instance
                object = className.getDeclaredConstructor(parameters).newInstance(speed);
            }
            return (Car) object;
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException buildCarReflexionException)
        {
            buildCarReflexionException.printStackTrace();
        }
        return null;
    }
}
