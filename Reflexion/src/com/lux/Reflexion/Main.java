package com.lux.Reflexion;

import com.lux.Reflexion.car.Car;
import com.lux.Reflexion.car.CarFactory;
import com.lux.Reflexion.car.Watchable;

import java.util.ArrayList;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        // Creating cars
        List<Car> cars = new ArrayList<Car>();
        addCarsInstantiation(cars);
        addCarsReflexion(cars);
        addCarsMeta(cars);
        // Moving and watching cars
        try
        {
            int x = 0;
            while (x++ < 20)
            {
                for (Car v : cars)
                {
                    v.move();
                    System.out.println(v);
                    watchCar(v);
                }
                System.out.println("");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // Adding by instantiation 3 cars
    private static void addCarsInstantiation(List<Car> cars)
    {
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                INSTANTIATION, true, 0));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                INSTANTIATION,false, 50));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                INSTANTIATION,false, 10));
    }

    // Adding by meta 3 cars
    private static void addCarsMeta(List<Car> cars)
    {
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                META, true, 0));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                META,false, 50));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                META,false, 10));
    }

    // Adding by reflexion 3 cars
    private static void addCarsReflexion(List<Car> cars)
    {
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                REFLEXION,true, 0));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                REFLEXION, false, 50));
        cars.add(CarFactory.buildCar(CarFactory.ConstructionMode.
                REFLEXION, false, 10));
    }

    // Watching car
    private static void watchCar(Car v) throws Exception
    {
        // Car's position exceed 1000 and is watchable statement
        if (v.getPosition() > 1000 && v instanceof Watchable)
        {
            int exceed = ((Watchable)v).watch(60);
            // Car exceed statement
            if (exceed > 10)
            {
                throw new Exception("ID : " + v.getId() + " --> exceed " + exceed + " (" + v.getClass() + ")");
            }
        }
    }
}
